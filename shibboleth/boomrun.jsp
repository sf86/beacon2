<script type="application/javascript">
  var isAgent = {
    OutlookApp: function() {
      return navigator.userAgent.match(/iPhone/);
    },
    any: function() {
        return (isAgent.OutlookApp());
    }
  };
  (function() {
    if( isAgent.any() ) { return; };
    // random beacon sampling
    var epoch = Math.floor(Date.now() / 1000);
    var urls = ["https://beacon.oit.duke.edu"];
    var num = Math.floor(Math.random() * urls.length);
    host = urls[num].replace(new RegExp('http.*://',''),'');
    BOOMR.init({
         beacon_url: "https://via.oit.duke.edu/boomerangs/boompost",
         beacon_type: 'POST',
         BW: {
           base_url: urls[num] + '/images/',
           cookie: "<%=request.getServerName()%>-bw",
           cookie_exp: 120,
           test_https: true,
           nruns: 5,
           block_beacon: true
         },
         RT: {
           cookie: "<%=request.getServerName()%>-rt",
           cookie_exp: 120,
           test_https: true,
           block_beacon: true
         }
    }).
    addVar({
      "clientip": "<%=request.getRemoteAddr()%>",
      "useragent": "<%=request.getHeader("User-Agent")%>",
      "http_ver": "<%=request.getProtocol()%>",
      "method": "<%=request.getMethod()%>",
      "req_time": epoch,
      "uri_path": "<%=request.getRequestURI()%>",
      "host": host,
      "protocol": "<%=request.getScheme()%>"
    });
  })();
</script>
