<html>
<%@ page contentType="text/html; charset=utf-8" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title></title>
  <script src="<%= request.getContextPath() %>/js/boomerang-oit.js" type="text/javascript"></script>
  <script type="text/javascript">
    var myip = '';
  </script>
  <script type="application/javascript">
    function getip(json){
      myip = json.ip;
    }
  </script>
  <script type="application/javascript" src="https://via.oit.duke.edu/myip"></script>
  <script type="application/javascript">
    var isAgent = {
      OutlookApp: function() {
        return navigator.userAgent.match(/iPhone/);
      },
      any: function() {
          return (isAgent.OutlookApp());
      }
    };
    (function() {
      if( isAgent.any() ) { return; };
      // random beacon sampling
      var epoch = Math.floor(Date.now() / 1000);
      var urls = ["https://beacon.oit.duke.edu"];
      var num = Math.floor(Math.random() * urls.length);
      host = urls[num].replace(new RegExp('http.*://',''),'');
      BOOMR.init({
           beacon_url: "https://via.oit.duke.edu/boomerangs/boompost",
           beacon_type: 'POST',
           BW: {
             base_url: urls[num] + '/images/',
             cookie: "<%=request.getServerName()%>-bw",
             cookie_exp: 120,
             //cookie: null,
             test_https: true,
             nruns: 3,
             block_beacon: true
           },
           RT: {
             cookie: "<%=request.getServerName()%>-rt",
             cookie_exp: 120,
             //cookie: null,
             test_https: true,
             block_beacon: true
           }
      }).
      addVar({
        "clientip": myip,
        "useragent": "<%=request.getHeader("User-Agent")%>",
        "http_ver": "<%=request.getProtocol()%>",
        "method": "<%=request.getMethod()%>",
        "req_time": epoch,
        "uri_path": "<%=request.getRequestURI()%>",
        "host": host,
        "protocol": "<%=request.getScheme()%>"
      });
    })();
  </script>
</head>
<body>
</body>
</html>
