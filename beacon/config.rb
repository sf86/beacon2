# Default to development if environment is not set.
saved = environment
if (environment.nil?)
  environment = :development
else
  environment = saved
end

# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# You can select your preferred output style here (:expanded, :nested, :compact
# or :compressed).
output_style = (environment == :expanded) ? :expanded : :nested

# To enable relative paths to assets via compass helper functions. 
relative_assets = true

# Conditionally enable line comments when in development mode.
line_comments = (environment == :production) ? true : false

# Output debugging info in development mode.
sass_options = (environment == :production) ? {} : {:debug_info => false}
