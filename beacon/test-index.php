<!DOCTYPE HTML>
<html>
<head>
<title>Via</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
</head>
<body>

<p id="results">
</p>

<script src="boomerang/boomerang.js" type="text/javascript"></script>
<script src="boomerang/plugins/bw.js" type="text/javascript"></script>
<script src="boomerang/plugins/navtiming.js" type="text/javascript"></script>
<script src="boomerang/plugins/rt.js" type="text/javascript"></script>
<script src="boomerang/howtos.js" type="text/javascript"></script>
<script src="js/main.js"></script>
<script type="application/javascript">
	function getip(json){
	 	document.write("My IP is: ", json.ip);
        myip = json.ip;
    }
</script>
<script type="application/javascript" src="http://localhost/myip.php"></script>
<script type="text/javascript">
BOOMR.init({
          //beacon_url: "https://beacon.oit.duke.edu/pi.php",
          beacon_url: "https://via.oit.duke.edu/boomerangs/boompost",
          //beacon_type: "POST",
          //DNS: {
            //base_url: "http://via.oit.duke.edu/bimages/",
            //base_url: "https://beacon.oit.duke.edu/images/",
            //block_beacon: true
          //},
          BW: {
            //base_url: "http://via.oit.duke.edu/bimages/",
            base_url: "http://<?php echo $_SERVER['SERVER_NAME'] ?>/images/",
            //cookie: "<?php echo $_SERVER['SERVER_NAME']-BW ?>",
            cookie: null,
            cookie_exp: 120,
            block_beacon: true
          },
          RT: {
            //cookie: "<?php echo $_SERVER['SERVER_NAME']-RT ?>",
            cookie: null,
            cookie_exp: 120,
            block_beacon: true
          }
    }).
    addVar({
      //"clientip": "<?php echo $_SERVER['REMOTE_ADDR'] ?>",
      "clientip": myip,
      "useragent": "<?php echo $_SERVER['HTTP_USER_AGENT'] ?>",
      "http_ver": "<?php echo $_SERVER['SERVER_PROTOCOL'] ?>",
      "method": "<?php echo $_SERVER['REQUEST_METHOD'] ?>",
      "req_time": "<?php echo $_SERVER['REQUEST_TIME'] ?>",
      "uri_path": "<?php echo $_SERVER['REQUEST_URI'] ?>",
      "host": "<?php echo $_SERVER['SERVER_NAME'] ?>",
    });
</script>
</body>
</html>
