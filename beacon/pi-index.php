<!DOCTYPE HTML>
<html>
<head>
<title>Boomerang</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
</head>
<body>

<p id="results">
</p>

<script src="boomerang/boomerang.js" type="text/javascript"></script>
<script src="boomerang/plugins/bw.js" type="text/javascript"></script>
<script src="boomerang/plugins/navtiming.js" type="text/javascript"></script>
<script src="boomerang/plugins/rt.js" type="text/javascript"></script>
<script src="boomerang/howtos.js" type="text/javascript"></script>
<script src="js/main.js"></script>
<script type="text/javascript">
BOOMR.init({
                beacon_url: "https://beacon.oit.duke.edu/pi.php",
                user_ip: "<?php echo $_SERVER['REMOTE_ADDR'] ?>",
                BW: {
                        base_url: "https://beacon.oit.duke.edu/images/",
                        cookie: 'HOWTO-BA'
                },
                RT: {
                        cookie: 'HOWTO-RT'
                }
        });
</script>
</body>
</html>
<!--
    Copyright (c) 2011, Yahoo! Inc.  All rights reserved.
    Copyrights licensed under the BSD License. See the accompanying LICENSE.txt file for terms.
-->

