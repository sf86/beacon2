Hello Developer!

If you are wondering 'Whats with these class names?'

We are using SMACSS methodology like all the cool kids do.

Example: l-somegthing means layout. Read this if you are interested http://smacss.com/book/type-layout

Also we wrap the header and footer in IDs and a lot of classes to make sure that the specifiity of the header and footer classes don't get overriden by other stylesheets that might be added for your specific project.

-----

FYI - This project was created using SASS.

If you said 'What's SASS?':
Its ok.  Just delete the '/sass' folder and the config.rb file and go on your happy way.

SASS Users:
Remember to make your edits in the SASS folder so your CSS files do not get overridden when you comple your SASS.  Also you can edit the config.rb file to add line numbers and all that jazz.

