FROM docker.io/nginx:latest
RUN apt-get update && apt-get upgrade -y
### Set up our docker user
RUN echo 'docker-user:x:1000:0:docker container user:/home/docker-user:/bin/bash' >> /etc/passwd
RUN mkdir -p ~docker-user && chown -R docker-user:0 ~docker-user && chmod g+w ~docker-user && chown docker-user /tmp
ENV TZ America/New_York
RUN rm /etc/nginx/conf.d/default.conf
WORKDIR /beacon
COPY ./beacon /beacon
COPY ./beacon.conf /etc/nginx/conf.d/site.conf
# Set up dirs and permissions so we can run without root
RUN chmod -Rf g+rwx /etc/passwd ; chgrp -R 0 /etc/passwd ;\
    true
CMD ["nginx", "-g", "daemon off;"]
