#!/bin/bash
set -e
set -x

if (( $# != 2 ))
then
  echo "Usage: ${0} <deployset> <hostname>" >&2
  exit 1
fi

TOPDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

APP_NAME=beacon
DEPLOY_SET=${1}
TARGET_FQDN=${2}

if [ ! -z ${SSH_DEPLOY_KEY+x} ]; then
  chmod 600 ${SSH_DEPLOY_KEY} # make ssh happy with the file permissions
  SSH_ARGS="-i ${SSH_DEPLOY_KEY}"
  echo "SSH Keyfile is ${SSH_DEPLOY_KEY}"
fi
SSH_ARGS="${SSH_ARGS} -o StrictHostKeyChecking=no"

TMP_DEPLOY_DIR=$(mktemp -d)

# unset to avoid conflict with systems' runners
unset DOCKER_HOST

cp -a ${TOPDIR}/docker-compose.yml ${TOPDIR}/deploy/common/* ${TMP_DEPLOY_DIR}
cp -Ra ${TOPDIR}/beacon ${TMP_DEPLOY_DIR}/beacon
# Save environment variables from gitlab-ci so they can be used in docker-compose
for x in DEPLOYMENT_ENV_NAME PIPELINE_IMAGE ENV_NAME SITENAME TITLE_STRING; do
  echo "${x}=${!x}" >> ${TMP_DEPLOY_DIR}/.env
done
# Make sure permissions aren't too open on files we copy over
chmod -R go-w ${TMP_DEPLOY_DIR}
ls -laR ${TMP_DEPLOY_DIR}
# Copy the files over and restart containers
chmod -R og-w ${TMP_DEPLOY_DIR} # work around broken gitlab-ci umask
scp ${SSH_ARGS} -B -pr ${TMP_DEPLOY_DIR}/* ${TMP_DEPLOY_DIR}/.env automation-gitlab-ci@${TARGET_FQDN}:/srv/docker-compose/${APP_NAME}
ssh ${SSH_ARGS} -o BatchMode=yes automation-gitlab-ci@${TARGET_FQDN} "docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} && cd /srv/docker-compose/${APP_NAME} && docker-compose pull && docker-compose up -d --remove-orphans && docker image prune -af"

if [[ ! -z ${TMP_DEPLOY_DIR+x} && -d ${TMP_DEPLOY_DIR} ]]
then
  rm -rf ${TMP_DEPLOY_DIR}
fi

if [ ! -z ${SSH_KEY_FILE+x} ]; then
  rm -f ${SSH_KEY_FILE}
fi
