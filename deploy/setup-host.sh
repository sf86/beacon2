#!/bin/bash

# This script is intended to be run on the host running the application before we deploy
# to it
set -e

APP_NAME=beacon
SERVICE_ACCOUNT=automation-gitlab-ci
SSH_PUB_KEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJtuzuqpueRQlUhJdOvfxHwdZXkIYqf0VwH1F21SpaCh beacon@git-internal"

# Install docker (must happen early to get group created)
sudo apt-get install -y docker docker-compose apg

# Configure journald logging for docker
if [ ! -f /etc/docker/daemon.json ]
then
  sudo sh -c "cat > /etc/docker/daemon.json" << EOF
{ "log-driver": "journald" }
EOF
  sudo systemctl restart docker
fi

# This binary breaks 'docker login' via script over ssh.  Move it out of the way and
# everything will work
if [ -f /usr/bin/docker-credential-secretservice ]
then
  sudo mv /usr/bin/docker-credential-secretservice /usr/bin/docker-credential-secretservice.broken
fi

# Setup CI service account to connect in
sudo mkhomedir_helper	${SERVICE_ACCOUNT}
SA_HOMEDIR=$(eval echo ~${SERVICE_ACCOUNT})
sudo mkdir -p ${SA_HOMEDIR}/.ssh
sudo chown ${SERVICE_ACCOUNT} ${SA_HOMEDIR}/.ssh
sudo sh -c "echo ${SSH_PUB_KEY} > ${SA_HOMEDIR}/.ssh/authorized_keys2"
sudo chmod 700 ${SA_HOMEDIR}/.ssh
sudo chmod 600 ${SA_HOMEDIR}/.ssh/authorized_keys2

# Setup firewall rules
#sudo ufw allow from 10.138.12.149 to any port 22 proto tcp comment automation-ci-01
#sudo ufw allow from 10.138.12.185 to any port 22 proto tcp comment automation-ci-02

# Give service account access to docker
sudo usermod -a -G docker ${SERVICE_ACCOUNT}
# Exempt service account from duo
sudo usermod -a -G duo_exempt ${SERVICE_ACCOUNT}

# Setup project directory
sudo mkdir -p /srv/docker-compose/${APP_NAME}
sudo chown ${SERVICE_ACCOUNT} /srv/docker-compose/${APP_NAME}

# this can stay owned by root
sudo mkdir -p /srv/docker-compose/${APP_NAME}/certbot-webroot

# Setup data directory
sudo mkdir -p /srv/docker-compose/${APP_NAME}/data
sudo chown 1000 /srv/docker-compose/${APP_NAME}/data

# Setup backups directory
#sudo mkdir -p /srv/docker-compose/${APP_NAME}/backups
#sudo chown 1000 /srv/docker-compose/${APP_NAME}/backups

# Create log directory
sudo mkdir -p /srv/docker-compose/${APP_NAME}/log
sudo chown 1000 /srv/docker-compose/${APP_NAME}/log

#sudo mkdir -p /srv/docker-compose/${APP_NAME}/mysql

#sudo chmod o-rx /srv/docker-compose/${APP_NAME}/{data,backups,mysql}
sudo chmod o-rx /srv/docker-compose/${APP_NAME}/data

# Setup log rotate
sudo sh -c "cat > /etc/logrotate.d/${APP_NAME}" << EOF
/srv/docker-compose/${APP_NAME}/log/*.log {
    rotate 30
    copytruncate
    daily
    compress
    delaycompress
    notifempty
    missingok
}
EOF

# Setup stub docker-compose.yml so the service will work properly
if [ ! -f /srv/docker-compose/${APP_NAME}/docker-compose.yml ];
then
  version='version: "3"'
  sudo sh -c "echo ${version} > /srv/docker-compose/${APP_NAME}/docker-compose.yml"
  sudo chown ${SERVICE_ACCOUNT} /srv/docker-compose/${APP_NAME}/docker-compose.yml
fi

